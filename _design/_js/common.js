var collapse_text1_num = 0, collapse_text2_num = 0;
var selected_areas_data = $.cookie('selected_areas_data') ? JSON.parse($.cookie('selected_areas_data')) : null;
var sa_list = selected_areas_data ? selected_areas_data.list : new Array();
var sa_order_by = (selected_areas_data && selected_areas_data.order_by) ? selected_areas_data.order_by : 'settlement';
var sa_order_direct = selected_areas_data ? selected_areas_data.order_direct : 'ASC';
var test_hover_advanced_link_var = false;
var reset_click = function() {
    reform_selected_areas($(this).parent().parent().attr('area_id'), 'remove');
    if (sa_list.length) {
        $(this).parent().parent().fadeOut(600, function() {
            $(this).remove();
            $('#top_place_block .selected_areas table tr:even').css('background-color', '#ffd800');
            $('#top_place_block .selected_areas table tr:odd').css('background-color', '#ffef94');
        });
    } else {
        $('#top_place_block .selected_areas table').fadeOut(600, function() {
            $(this).fadeOut('fast');
            $('#top_place_block .reset_all, #top_place_block .request_all').hide();
        });
    }
    $('.cnt_s_areas').html(sa_list.length);
	$('.price tr[area_id='+$(this).parent().parent().attr('area_id') +'] .plus').removeClass('plus_selected');
    if ($('.price .plus_selected').length) move_free_request_button(); else $('.settlement .free_request').fadeOut('fast');
    return false;
}


/**
 * Проверка, был ли человек на сайте
 * первая проверка через значение wasHere в localStorage (localStorage используется потому, что проверка по куке wasHere не достоверна - кука может отсутствовать, даже при повторных заходах)
 * если localStorage не поддерживается, дополнительная проверка идет по наличию куки wasHere  
 * @returns {Boolean}
 */
function wasHere(){
	var flag = false; 
	if (typeof(Storage) != "undefined") { // если localStorage поддерживается браузером 
		if (localStorage.getItem("wasHere")){ // если wasHere есть в localStorage
			flag = true;
		} else {
			localStorage.setItem("wasHere", 1); // записываем wasHere = 1 в localStorage
			flag = false;
		}
	} else {
		if ($.cookie('wasHere')){ //  || $.cookie('unique_number') - удалено из условия, т.к. проверка на наличие unique_number проходит даже срезу после удаления этой куки
			flag = true;
		} else {
			flag = false;
		}
	}
	return flag;
}

//Получение значения куки по имени (логирование)
function getCookie(cname) {
    var name = cname + "=",
    	ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}

// отправка данных о сессии (логирование)
function addVisitData(){
	data.winHWidth = $(window).width(); // var data в чанке log_page_view
    data.winHeight = $(window).height();

	$.ajax({
		type: "POST",
		url: "/core/_snippet/log_visit_session.php",
		data: {data: data}
	});
}

// логирование

// при уходе со стр. отправляем данные для логирования (времени ухода)
window.onbeforeunload = function() {
	if (window.data) {
		// чтобы Firefox выполнил ajax запрос по событию beforeunload, запрос д.б. не асинхронным
		var isAsync = (data.browser == "Firefox" || data.browser == "Safari") ? false : true,
			upd_data = {
				page_url: data.page_url,
				uuid: data.uuid,
				visitId: data.visitId
		};
		
	    $.ajax({
	    	type: "POST",
	    	async: isAsync,
	    	url: "/core/_snippet/log_visit_page_update.php",
	    	data: {data: upd_data}
	    });
	}
};


$(function() {
	//$('.sovet').hover(function() { helpBox_on($(this).attr('data-title')); }, function() { helpBox_off() } );
	$( "#content" ).on( "mouseenter", ".sovet", function() { helpBox_on($(this).attr('data-title')); });
	$( "#content" ).on( "mouseleave", ".sovet", function() { helpBox_off(); });
	
	$('#top_place_block .reset_all, #top_place_block .request_all').hide();
	collapse_text();
	$('#header .logo a').hover(function() { logo_in(); }, function() { logo_out(); } );
	
	// вывод бокса "Посмотрите полезную информацию, скрытую здесь."  
	//setTimeout('invite2()',10000);
	
	setTimeout('invite()',120000);
	setTimeout('invite()',300000);
	
	// показ левой панели "Звонок с сайта / Поселки"
	if ( (wasHere() == false) && (document.referrer == '' || !(document.referrer.indexOf('blizoki') + 1))) { // !!! ф-я wasHere() должна идти первым условием, т.к. она пишет значение wasHere в localStorage
		$('#advanced_block').bind('mouseenter', test_hover_advanced_link_true);
		$('#advanced_block').bind('mouseleave', test_hover_advanced_link_false);
		setTimeout('a_block()',3000);
		setTimeout('a_block_hide_test()',8000);
	}
	$('#advanced_link').click(function() {
		if ($('#advanced_block .chat').is(':hidden')) a_block();
		else a_block_hide();
	});
	$('#top_place_link').click(function() { top_place_show(); });
	$('#top_place_block .close').click(function() {
		$('#top_place_block').css('min-height', '');
		$('#top_place_block').slideUp(600);
		$('#advanced_block').animate({top: 0}, 600);
		if (navigator.userAgent.indexOf('MSIE 8.0') != -1) al_top = 0; else al_top = 164;
		$('#advanced_link').animate({top: al_top}, 600);
		return false;
	});
	if (navigator.userAgent.indexOf('MSIE 8.0') != -1) {
		$('.main_menu li').eq(3).css('margin-right', 0);
		$('.main_menu li').eq(7).css('margin-right', 0);
	}
	$('#top_place_block .request_all').click(function() {		
		$(this).fadeOut('fast', function() {
			$('#top_place_block .caption').fadeOut('slow', function() { $('#top_place_block .request_form').fadeIn('slow'); });
		});
	});	
	/* начало ОБРАБОТКИ ДВУХ ФОРМ */	
	$('.settlement .free_request').click(function() {	
		top_place_show();
		$('#top_place_block .request_all').fadeOut('fast', function() {
			$('#top_place_block .caption').fadeOut('slow', function() { $('#top_place_block .request_form').fadeIn('slow'); });
		});		
		 
		return false; 
		});
		$('#top_place_block .request_form input[name="fio"]').focus(function() { if ($(this).val() == 'ФИО*') $(this).val('').focus(); });
		$('#top_place_block .request_form input[name="fio"]').blur(function() { if (!$(this).val()) $(this).val('ФИО*'); });	
		$('#top_place_block .request_form input[name="phone"]').focus(function() { if (!$(this).val() || $(this).val() == 'ТЕЛЕФОН*') $(this).mask('+7 (999) 999-99-99'); });
		$('#top_place_block .request_form input[name="phone"]').blur(function() {
			if ($(this).val().replace(/[^\d.]/g, '').length != 11) {
				$(this).unmask('+7 (999) 999-99-99');
				setTimeout(function() { $('#top_place_block .request_form input[name="phone"]').val('ТЕЛЕФОН*'); }, 6);
			}
	});	
	$('#top_place_block .request_form input[name="email"]').focus(function() { if ($(this).val() == 'E-MAIL*') $(this).val('').focus(); });
	$('#top_place_block .request_form input[name="email"]').blur(function () { if (!$(this).val()) $(this).val('E-MAIL*'); });
	$('#top_place_block .request_form .cancel').click(function() {
		$('#top_place_block .request_form').fadeOut('fast', function() {
			$('#top_place_block .caption').fadeIn('slow');
			$('#top_place_block .request_all').fadeIn('slow');
		});
		return false;
	});
	$('#top_place_block .request_form').submit(function() {
        var flag = true;
        var message = null;
        if ($('#top_place_block input[name="fio"]').val().length < 3 || $('#top_place_block input[name="fio"]').val() == 'ФИО*') {
            flag = false; $('#top_place_block input[name="fio"]').focus(); message = 'Не заполнено поле ФИО!';
        } else if ($('#top_place_block input[name="phone"]').val().replace(/[^\d.]/g, '').length != 11) {
            flag = false; $('#top_place_block input[name="phone"]').focus(); message = 'Не заполнено поле ТЕЛЕФОН!';
        } else if ($('#top_place_block input[name="email"]').val().length < 6 || $('#top_place_block input[name="email"]').val() == 'E-MAIL*') {
            flag = false; $('#top_place_block input[name="email"]').focus(); message = 'Не заполнено поле E-MAIL!';
        }
        if (message) alert(message);
		if(flag) {
			$('.request_form .submit').prop('disabled', true);
			// удаляем
			write_s_cookie();
			$('.cnt_s_areas').html('0');
			//$('#top_place_block .close').click();
			//sa_list = [];
			//sa_order_by = null;
			//sa_order_direct = null;
			$('#top_place_block .selected_areas table tr:not(.head)').remove();
			$('.price .plus_selected').removeClass('plus_selected');
			$('.settlement .free_request').fadeOut('fast');
			$('#top_place_block table').hide();
			$('#top_place_block .reset_all, #top_place_block .request_all, #top_place_block .request_form').hide();
			
		}		
		
        return flag;
    });	
	//$('.price .request').click(function() {
	$( "#content" ).on( "click", ".price .request", function() {
		$('#request_form .selected_areas').html(sa.join(', '));
		$('#request_form input[name="selected_areas"]').val(sa.join(', '));
		if ($(this).hasClass('disabled_request')) return false;
		$('#fon').fadeIn('slow');
		$('#request_form').fadeIn('slow');
	});
    $('#request_form .close').click(function() { $('#request_form').fadeOut('fast'); $('#fon').fadeOut('fast'); });	
	$('#request_form input[name="fio"]').focus(function() { if ($(this).val() == 'ИМЯ*') $(this).val('').focus(); });
	$('#request_form input[name="fio"]').blur(function () { if (!$(this).val()) $(this).val('ИМЯ*'); });
	$('#request_form input[name="phone"]').focus(function() { if (!$(this).val() || $(this).val() == 'ТЕЛЕФОН') $(this).mask('+7 (999) 999-99-99'); });
	$('#request_form input[name="phone"]').blur(function() {
		if ($(this).val().replace(/[^\d.]/g, '').length != 11) {
			$(this).unmask('+7 (999) 999-99-99');
			setTimeout(function() { $('#request_form input[name="phone"]').val('ТЕЛЕФОН'); }, 6);
		}
	});
	$('#request_form input[name="email"]').focus(function() { if ($(this).val() == 'E-MAIL') $(this).val('').focus(); });
	$('#request_form input[name="email"]').blur(function () { if (!$(this).val()) $(this).val('E-MAIL'); });		
	$('#request_form textarea').focus(function() { if ($(this).val() == 'Дополнительные сведения') $(this).val('').focus(); });
	$('#request_form textarea').blur(function() { if (!$(this).val()) { $(this).val('Дополнительные сведения'); } });	
    $('#request_form').submit(function() {
		
        var flag = true;
        var message = null;
        if ($('#request_form input[name="fio"]').val().length < 3 || $('#request_form input[name="fio"]').val() == 'ИМЯ*') {
            flag = false; $('#request_form input[name="fio"]').focus(); message = 'Не заполнено поле ИМЯ!';$('#request_form input[name="fio"]').css('border','2px solid #000');
        } else if( ($('#request_form input[name="phone"]').val().replace(/[^\d.]/g, '').length != 11) && ($('#request_form input[name="email"]').val().length < 6 || $('#request_form input[name="email"]').val() == 'E-MAIL') ){
            flag = false; $('#request_form input[name="phone"]').focus(); message = 'Не заполнено поле E-MAIL или ТЕЛЕФОН!'; $('#request_form input[name="phone"]').css('border','2px solid #000');
        }
        if (message) {
        	//alert(message);
        	$('#extra_text_form').addClass('extra_text_form');
        	$('#extra_text_form').html("Вы указали не все данные, мы не сможем связаться с Вами. Вы можете заново заполнить форму или  <a onclick=\"window.open(this.href+'?referrer='+escape(window.location.href)+'&extra=to%3A74956648336%3BtoHash%3Ad2f2e2a9d8d9aca6126647f535e50ac8014f5459', '_blank', 'width=236,height=220,resizable=no,toolbar=no,menubar=no,location=no,status=no'); return false\" href=\"http://zingaya.com/widget/838df5b6d9b74d558ea7c520a5c8d8b4\">позвонить</a> или <a href=\"#\" onclick=\"a_block();load_iframe(); $('#request_form').fadeOut('fast'); $('#fon').fadeOut('fast');\">написать</a> эксперту online.")
        }
		if(flag) $('#request_form .submit').prop('disabled', true);
        return flag;
    });	
	/* конец ОБРАБОТКИ ДВУХ ФОРМ */	
	/*$('#advanced_block .chat_button').click(function() {
		$('#advanced_block .chat_button').fadeOut(300, function() { $('#advanced_block .chat form').fadeIn(600); });
		return false;
	});
	$('#advanced_block .chat .close').click(function() {
		$('#advanced_block .chat form').fadeOut(300, function() { $('#advanced_block .chat_button').fadeIn(600); });
		return false;
	});*/
    $('.fancybox, a[rel=fancybox]').fancybox({
        padding: 6,
        margin: 15,
		transitionIn: 'fade',
		transitionOut: 'fade',
		easingIn: 'none',
		easingOut: 'none',
        width: 'auto',
        height: 'auto',
        tpl: {
            closeBtn: '<a title="Закрыть (Esc)" class="fancybox-item fancybox-close" href="javascript:;"></a>',
            next: '<a title="Следующее фото (Right)" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
            prev: '<a title="Предыдущее фото (Left)" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
        }
    });
    //$('.price .plus').click(function() {
    $( "#content" ).on( "click", ".price .plus", function() {
        free_request = $('.settlement .free_request');
        if (!$(this).hasClass('plus_selected')) {
			$(this).addClass('plus_selected');
			reform_selected_areas($(this).parent().parent().attr('area_id'), 'add');
			$(this)  //ID Картинки, летящей в корзину
			.clone()
			.css({'position' : 'absolute', 'z-index' : '9999', 'width' : '40px', 'height' : '40px'})  //Начальные значения для клона, ширину/высоту задаём для хрома, right/top -начальное положение картинки
			.prependTo($(this).parent('td'))  //место появления клона картинки, можете изменить на эту же картинку
			.animate({top: 20 }, 200, function() {  //top/right место корзины, 800 - скорость
				$(this).remove();
			});	
			
		}
        else { $(this).removeClass('plus_selected');reform_selected_areas($(this).parent().parent().attr('area_id'), 'remove');}
		
        if (!free_request.is(':visible') && $('.price .plus_selected').length) free_request.fadeIn('slow', function() { move_free_request_button(); });
        else if (free_request.is(':visible') && !$('.price .plus_selected').length) free_request.fadeOut('fast', function() { move_free_request_button(); });
        else move_free_request_button();
		
        reload_selected_areas_data();
		if (sa_list.length && $('#top_place_block .request_all').is(':hidden')) $('#top_place_block .request_all').show();
        $('.cnt_s_areas').html(sa_list.length);
        return false;
    });
    $('#top_place_block .selected_areas .head a').click(function() {
        if (sa_order_by == $(this).attr('fname') && $(this).hasClass('asc')) {
            sa_order_direct = 'DESC';
            $(this).removeClass('asc').addClass('desc');
        } else if (sa_order_by == $(this).attr('fname') && $(this).hasClass('desc')) {
            sa_order_direct = 'ASC';
            $(this).removeClass('desc').addClass('asc');
        } else {
            $('#top_place_block .selected_areas .head a').removeClass('asc').removeClass('desc');
            sa_order_by = $(this).attr('fname');
            sa_order_direct = 'ASC';
            $(this).addClass('asc');
        }
        write_s_cookie();
        reload_selected_areas_data('order');
        return false;
    });
    if (sa_order_by) $('#top_place_block .selected_areas .head a[fname="'+sa_order_by+'"]').addClass(((sa_order_direct == 'ASC') ? 'asc' : 'desc'));
    $('#top_place_block .reset_all').click(function() {
        $('.cnt_s_areas').html('0');
        $('#top_place_block .close').click();
        sa_list = [];
        sa_order_by = null;
        sa_order_direct = null;
        $('#top_place_block .selected_areas table tr:not(.head)').remove();
        $('.price .plus_selected').removeClass('plus_selected');
        $('.settlement .free_request').fadeOut('fast');
        $('#top_place_block table').hide();
        $('#top_place_block .reset_all, #top_place_block .request_all, #top_place_block .request_form').hide();
        write_s_cookie();
    });

    if (sa_list.length) {
        reload_selected_areas_data();
        $('.cnt_s_areas').html(sa_list.length);
        $('.settlement .free_request').fadeIn('slow', function() { move_free_request_button(); });
    }
    
    // логирование
    
    // определение, первая ли это просматриваемая стр. в данном сеансе работы
	data.firstPage = (data.visitId != getCookie('PHPSESSID')) ? true : false;
	
	// сохранение данных
	addVisitData();    
    
});
function top_place_show() {
	$('#top_place_block .caption').show();
	minheight = (!$('body').hasClass('main') && sa_list.length) ? 285 : 220;
	$('#top_place_block').slideDown(600, function() { $(this).css('min-height', minheight); } );
	$('#advanced_block').animate({top: 220}, 600);
	if (navigator.userAgent.indexOf('MSIE 8.0') != -1) al_top = 165; else al_top = 322;
	$('#advanced_link').animate({top: al_top}, 600);
}
function a_block() {
	$('.sh_operator_help').hide();
	$('.sh_operator_help2').hide();
	if (!$('#advanced_block .chat').is(':visible')) {
		if (navigator.userAgent.indexOf('MSIE 8.0') != -1) al_left = 458; else al_left = 289;
		$('#advanced_link').animate({left: al_left}, 600);
		$('#advanced_block').animate({width: 460}, 600, function() {
			if ($('#advanced_link').css('left') == al_left+'px') $('#advanced_block .chat, #advanced_block .call, #advanced_block .settlements').fadeIn('slow');
			$('#advanced_link').addClass('advanced_link_active');
		});
	}
	if ($('#top_place_block').is(':visible')) {
		$('#advanced_block').animate({top: 0}, 600);
		if (navigator.userAgent.indexOf('MSIE 8.0') != -1) al_top = 0; else al_top = 164;
		$('#advanced_link').animate({top: al_top}, 600, function() { $('#top_place_block').css('min-height', ''); $('#top_place_block').slideUp(300); });
	}
	$('#advanced_link').unbind('mouseenter mouseleave');
}
function a_block_hide() {
	$('#advanced_block .chat, #advanced_block .call, #advanced_block .settlements').hide();
	$('#advanced_block').animate({width: 8}, 600);
	$('#advanced_link').removeClass('advanced_link_active');
	if (navigator.userAgent.indexOf('MSIE 8.0') != -1) al_left = 8; else al_left = -165;
	$('#advanced_link').animate({left: al_left}, 600);
}			
function a_block_hide_test() {
	if(test_hover_advanced_link_var == false) {
		$('#advanced_block .chat, #advanced_block .call, #advanced_block .settlements').hide();
		$('#advanced_block').animate({width: 8}, 600);
		$('#advanced_link').removeClass('advanced_link_active');
		if (navigator.userAgent.indexOf('MSIE 8.0') != -1) al_left = 8; else al_left = -165;
		$('#advanced_link').animate({left: al_left}, 600);
	} else {
	setTimeout('a_block_hide_test()',1000);		
	}	
}
function test_hover_advanced_link_true() {
	test_hover_advanced_link_var = true;	
}
function test_hover_advanced_link_false() {
	test_hover_advanced_link_var = false;
}
function invite() {
	//$('.sh_operator_help2').hide();
	//if ($('#advanced_block .chat').is(':hidden')) {
	//	$('.sh_operator_help').show('slow');
//	}
}

function invite2() {
//	if (test_hover_advanced_link_var === false) {
//		$('.sh_operator_help2').show('slow');
//	}
}

function logo_in() {
	$('#header .logo a').queue("fx", []);
	$('#header .logo a').animate({ rotate_prop: 90 }, {
        step: function(now, tween) {
            if (tween.prop === 'rotate_prop') {
                $(this).css('-webkit-transform','rotate('+now+'deg)');
                $(this).css('-moz-transform','rotate('+now+'deg)'); 
				$(this).css('-ms-transform','rotate('+now+'deg)');
				$(this).css('-o-transform','rotate('+now+'deg)');
                $(this).css('transform','rotate('+now+'deg)');  
            }
        }, duration: 150, easing: 'linear', complete: function() {
			$('#header .logo a').css('opacity', '0.25');
			$('#header .logo a').css('background-position', '-70px 0px');
			$('#header .logo a').css('opacity', '1');
			$('#header .logo a').animate({ rotate_prop: 360 }, {
					step: function(now, tween) {
						if (tween.prop === 'rotate_prop') {
							$(this).css('-webkit-transform','rotate('+now+'deg)');
							$(this).css('-moz-transform','rotate('+now+'deg)'); 
							$(this).css('-ms-transform','rotate('+now+'deg)');
							$(this).css('-o-transform','rotate('+now+'deg)');
							$(this).css('transform','rotate('+now+'deg)');  
						}
					}, duration: 150, easing: 'linear'
			});
		}
    });
}
function logo_out() {
	$('#header .logo a').queue("fx", []);
	$('#header .logo a').animate({ rotate_prop: -90 }, {
        step: function(now, tween) {
            if (tween.prop === 'rotate_prop') {
                $(this).css('-webkit-transform','rotate('+now+'deg)');
                $(this).css('-moz-transform','rotate('+now+'deg)'); 
				$(this).css('-ms-transform','rotate('+now+'deg)');
				$(this).css('-o-transform','rotate('+now+'deg)');
                $(this).css('transform','rotate('+now+'deg)');  
            }
        }, duration: 150, easing: 'linear', complete: function() {
			$('#header .logo a').css('opacity', '0.25');
			$('#header .logo a').css('background-position', '0px 0px');
			$('#header .logo a').css('opacity', '1');
			$('#header .logo a').animate({ rotate_prop: -360 }, {
					step: function(now, tween) {
						if (tween.prop === 'rotate_prop') {
							$(this).css('-webkit-transform','rotate('+now+'deg)');
							$(this).css('-moz-transform','rotate('+now+'deg)'); 
							$(this).css('-ms-transform','rotate('+now+'deg)');
							$(this).css('-o-transform','rotate('+now+'deg)');
							$(this).css('transform','rotate('+now+'deg)');  
						}
					}, duration: 150, easing: 'linear'
			});
		}
    });
}
function collapse_text() {
   	setInterval(function() {
		next_num = (collapse_text1_num == 1) ? 0 : 1;
		if ($(window).scrollTop() < 275 && navigator.userAgent.indexOf('MSIE 8.0') != -1) {
			$('#main .collapse span em').eq(collapse_text1_num).toggle(300, function() { $('#main .collapse span em').eq(collapse_text1_num).toggle(300); });
			collapse_text1_num = next_num;
			$('#main .h1description em').eq(collapse_text2_num).toggle(400, function() { $('#main .h1description em').eq(collapse_text2_num).toggle(800); });
			collapse_text2_num = (collapse_text2_num == 1) ? 0 : 1;
		} else if ($(window).scrollTop() < 275 && navigator.userAgent.indexOf('MSIE 8.0') == -1) {
			widh1_1 = parseInt($('#main .collapse span em').eq(collapse_text1_num).css('width'));
			widh1_2 = parseInt($('#main .collapse span em').eq(next_num).css('width'));
			widh1 = parseInt($('#main .collapse').css('width'));
			$('#main .collapse span em').eq(collapse_text1_num).fadeOut(300, function() {
				$('#main .collapse').animate({width: widh1 - widh1_1 + widh1_2}, 300, function() {
					$('#main .collapse span em').eq(next_num).fadeIn(300);
					collapse_text1_num = next_num;
				});
			});
			$('#main .h1description em').eq(collapse_text2_num).fadeOut(400, function() {
				collapse_text2_num = (collapse_text2_num == 1) ? 0 : 1;
				$('#main .h1description em').eq(collapse_text2_num).fadeIn(800);
			});
		}
    }, 6000);
}
/*
Подстановка css в iframe для лайв чата 
var cssLink = document.createElement("link")
cssLink.href = "style.css";
cssLink.rel = "stylesheet";
cssLink.type = "text/css";
frames['frame1'].document.body.appendChild(cssLink);
*/
function write_s_cookie() {
    $.cookie('selected_areas_data', JSON.stringify({'list': sa_list, 'order_by': sa_order_by, 'order_direct': sa_order_direct}),  {expires: 365, path: '/'});
}
function reset_initial() {
    $.each($('#top_place_block .reset'), function() { $(this).bind('click', reset_click); });
}
function reform_selected_areas(area_id, action) {
    if (action == 'add') sa_list.push(area_id);
    else {
        var new_sa_list = new Array();
        $.each(sa_list, function(i, v) { if (v != area_id) { new_sa_list.push(v); } });
        sa_list = new_sa_list;
    }
    write_s_cookie();
}
function gp_change() {
	$("#gp_select_add").toggle();
	$("#gp_select_remove").toggle();

}
function reload_selected_areas_data(action) {
    //if (!sa_list.length) return;
	if (!sa_list.length) {
		$('#top_place_block .selected_areas table').fadeOut(600, function() {
			$(this).fadeOut('fast');
			$('#top_place_block .reset_all, #top_place_block .request_all').hide();
		});
	}
    if (!action) $('#top_place_block .selected_areas table tr:not(.head)').remove();
    $.getJSON('/core/_snippet/ajax.php?action=get_areas_data', function(data) {
        if (data) {
            if (!action) {
                $.each(data, function(i, v) {
                    $('#top_place_block .selected_areas table tbody').append('<tr area_id="'+v.id+'"><td>'+v.settlement+'</td><td>'+v.number+'</td><td>'+v.square+'</td><td>'+v.price+(v.house_type ? ' / '+v.house_type : '')+'</td><td>'+v.total+'</td><td><a href="#" class="reset"></a></td></tr>');
                });
            } else {
                i = 0;
                $.each($('#top_place_block .selected_areas table tr:not(.head)'), function() {
                    $(this).attr('area_id', data[i].id).html('<td>'+data[i].settlement+'</td><td>'+data[i].number+'</td><td>'+data[i].square+'</td><td>'+data[i].price+(data[i].house_type ? ' / '+data[i].house_type : '')+'</td><td>'+data[i].total+'</td><td><a href="#" class="reset"></a></td>');
                    i++;
                });
            }
        }
    }).success(function() {
        $('#top_place_block .selected_areas table tr:odd').css('background-color', '#ffef94');
        $('#top_place_block .selected_areas table').fadeIn('slow');
        $('#top_place_block .reset_all, #top_place_block .request_all').show();
        reset_initial();
    });
}
function move_free_request_button() {
    free_request = $('.settlement .free_request');    
    items = $('.price .plus_selected');
    lastItem = $('.price .plus_selected:last');
    last_index = items.index(lastItem);
    last_selected_index = $('.price .plus_selected').eq(last_index).parent().parent().attr('index');    
    fr_mtop = parseInt(free_request.css('margin-top'));
    fr_mtop_new = 62 + (last_selected_index * 36);
    if (free_request.is(':visible') && last_selected_index && fr_mtop != fr_mtop_new) free_request.animate({marginTop: fr_mtop_new+'px'}, 500);
}
function elems_reform(elems) { var new_elems = new Array(); $.each(elems, function() { new_elems.push($(this).attr('val')); }); return new_elems; }

function send_from_genplan(area_to_book) {
	$('#request_form .selected_areas').html(area_to_book);
	$('#request_form input[name="selected_areas"]').val(area_to_book);
	$('#fon').fadeIn('slow');
	$('#request_form').fadeIn('slow');
}

// Подсказка
function defPosition(event) {
    var x = y = 0;
    if (document.attachEvent != null) {
        x = window.event.clientX ;//+ (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
        y = window.event.clientY ;//+ (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
    } else if (!document.attachEvent && document.addEventListener) {
        x = event.clientX; ///+ window.scrollX;
        y = event.clientY ;//+ window.scrollY;
    }
    return {x:x, y:y};
}
document.onmousemove = function(event) {
    var event = event || window.event;
    document.getElementById('hint').style.left = defPosition(event).x + 15 + "px";
    document.getElementById('hint').style.top = defPosition(event).y + 20 + "px";
}

function helpBox_on(text) {
    document.getElementById('hint_text').innerHTML = text;
    document.getElementById('hint').style.width = "220px";
    document.getElementById('hint').style.border = "1px solid #ffd800";
    document.getElementById('hint_text').style.padding = "5px";
    document.getElementById('hint').style.display = "block";
}

function helpBox_off() {
    document.getElementById('hint_text').innerHTML = "";
    document.getElementById('hint').style.width = 0;
    document.getElementById('hint').style.border = 0;
    document.getElementById('hint_text').style.padding = 0;
}
