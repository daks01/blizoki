/*Phone Show*/
/*

var prefix = '<span class="prefix blue">(495) <\/span>',
    show_link = '<div style="font-size:13px; line-height:1.1; padding-left:1px; display:inline-block; cursor:pointer; text-decoration:underline;">Показать<br>телефон<\/div>',
    show_link_on_load = '<div style="font-size:13px; line-height:1.1; padding-left:1px; display:inline-block; cursor:pointer; text-decoration:underline;"><img src="/_design/_img/loader.gif" /><\/div>';

function showPhone(e){
    e.removeClass('phonecut');
    e.html(prefix + e.data('phonestart') + '-' + show_link_on_load);
    $.ajax({
        type: "POST",

        data: '&type=add',
        url: "/core/_snippet/crm_call.php?action=true",
        cache: false,
        success: function(ht) {
            dat = $.parseJSON(ht);

            e.html(prefix + dat.phone_s).removeClass('phonecut');
            $('input[name="site_phone"]').val(dat.phone);
            $('input[name="utm_metka"]').val(dat.utm);
            $('input[name="new_id"]').val(dat.new_id);
            $('input[name="referer_1"]').val(dat.referer_1);
            $('input[name="phone_id"]').val(dat.phone_id);
            setTimeout(function(){
                hidePhone (e);
            }, 120000);
        },

        fail: function( jqXHR, textStatus ) {



            e.html(prefix + '664-83-33').removeClass('phonecut');
            $('input[name="site_phone"]').val('74956648333');
            $('input[name="utm_metka"]').val('не загружено');
            $('input[name="new_id"]').val(0);
            $('input[name="referer_1"]').val('не загружено');
            $('input[name="phone_id"]').val(0);
            setTimeout(function(){
                hidePhone (e);
            }, 120000);

            $.ajax({
                type: "POST",
                data: '&message=не загружен телефон из колтрекинга '+ textStatus + jqXHR,
                url: "/core/_snippet/error_mail.php?action=true",
                cache: false,
                success: function(ht2) {
                }
            });

        },
        error: function( jqXHR, textStatus ) {



            e.html(prefix + '664-83-33').removeClass('phonecut');
            $('input[name="site_phone"]').val('74956648333');
            $('input[name="utm_metka"]').val('не загружено');
            $('input[name="new_id"]').val(0);
            $('input[name="referer_1"]').val('не загружено');
            $('input[name="phone_id"]').val(0);
            setTimeout(function(){
                hidePhone (e);
            }, 120000);

            $.ajax({
                type: "POST",
                data: '&message=не загружен телефон из колтрекинга '+ textStatus + jqXHR,
                url: "/core/_snippet/error_mail.php?action=true",
                cache: false,
                success: function(ht2) {
                }
            });

        }



    });
}

function hidePhone(e){
    e.addClass('phonecut');
    e.html(prefix + e.data('phonestart') + '-' + '<div style="font-size:13px; line-height:1.1; padding-left:1px; display:inline-block; cursor:pointer; text-decoration:underline;">Показать<br>телефон<\/div>');

    $.ajax({
        type: "POST",

        data: '&new_id=' + $('input[name="new_id"]').val() + '&site_phone=' + $('input[name="site_phone"]').val(),
        url: "/core/_snippet/crm_call_get_data.php?action=true",
        cache: false,
        success: function(ht2) {
            dat2 = $.parseJSON(ht2);
            if(dat2.call == -1) {
                $( "#dialog-form" ).dialog( "open" );
            }

        }
    });

    //e.html(prefix + e.data('phonestart') + '-' + show_link).addClass('phonecut');

}


*/


$(function() {
    $(".phonehref").click(function(e){
        e.preventDefault();
        if ($(this).hasClass("phonecut")){
            showPhone($(this));
        } else {
            // hidePhone($(this));
        }
    });
});


$(function() {
    $('[data-toggle]').click(function(){
        var self = $(this);
        self.toggleClass('active');
        $('[data-toggle-target='+self.data('toggle')+']').slideToggle();
    });
});


$(function() {
    $('[data-dialog-open]').click(function(){
        var targetEl = $(this).attr('href');
        $(targetEl).dialog( "open" );
        return false;
    });
});






/*Masked Phone Number*/
$(function() {
    $('body #phone').mask('+7 (999) 999-99-99');
    var dialog, form,

        name = $( "#name" ),
        phone = $( "#phone" ),
        allFields = $( [] ).add( name ).add( phone );

    function addUser(form_obj) {
        // validateForm();
        if(!$("#recall-form").valid()){
            return false;
        }

        var loader = $('#imgloader'),
            submit_btn = form_obj.find('input[type="submit"]');

        loader.show();
        submit_btn.attr('disabled','disabled');

        $.post(form_obj.attr('action'), form_obj.serialize(), function(a){
            loader.hide();
            submit_btn.removeAttr('disabled');
            // dialog.dialog( "close" );
            $('.block-result').animate({
                left: '-3px',
                opacity: '1'
            }, 500 );
        });
    }

    function otkaz() {
        $.ajax({
            type: "POST",

            data: '&new_id=' + $('input[name="new_id"]').val() + '&site_phone=' + $('input[name="site_phone"]').val(),
            url: "/core/_snippet/crm_call_get_data.php?action=otkaz",
            cache: false,
            success: function(ht2) {


            }
        });
    }

    dialogLightbox = $( "#dialog-form" ).dialog({
        autoOpen: false,
        //height: 300,
        width: 345,
        modal: true,
        buttons: {
            // "Перезвоните мне": addUser,
            //"Отмена": function() {

            //  dialogLightbox.dialog( "close" );
            //  otkaz();
            // }
        },
        close: function() {
            form[ 0 ].reset();
            allFields.removeClass( "ui-state-error" );
            otkaz();
        }
    });
    subscribeLightbox = $( "#subscribe-form" ).dialog({
        autoOpen: false,
        //height: 300,
        width: 345,
        modal: true,
        buttons: {},
        close: function() {
            form[ 0 ].reset();
            allFields.removeClass( "ui-state-error" );
        }
    });

    form = dialogLightbox.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
        addUser($(this));
    });

    $( "#create-user" ).button().on( "click", function() {
        dialogLightbox.dialog( "open" );
    });

    $( ".btn-close" ).click(function(e){
        dialogLightbox.dialog( "close" );
    })

    $("#recall-form").validate({
        rules: {
            name: "required",
            phone: "required",
        },
        messages: {
            name: "Поля обязательны к заполнению.",
            phone: "Поля обязательны к заполнению.",
        }
    });

});
